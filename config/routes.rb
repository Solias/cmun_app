Rails.application.routes.draw do
  
  resources :contacts, only: [:new, :create]
  
  root "static_pages#home"
  
  get '/home', to: 'static_pages#home'

  get '/committees', to: 'static_pages#committees'

  get '/registration', to: 'static_pages#registration'

  get '/schedule', to: 'static_pages#schedule'

  get '/studyguide', to: 'static_pages#studyguide'
   
  get '/contact', to: 'contacts#new'
  
  

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
end
