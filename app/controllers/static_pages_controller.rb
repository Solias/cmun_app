class StaticPagesController < ApplicationController
  def home
  end

  def committees
  end

  def registration
  end

  def schedule
  end

  def studyguide
  end
end
