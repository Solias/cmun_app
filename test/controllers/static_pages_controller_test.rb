require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @base_title = "CHSMUN '17"
  end



  test "should get home" do
    get root_path
    assert_response :success
    assert_select "title", "Home | #{@base_title}"
    
  end

  test "should get committees" do
    get committees_path
    assert_response :success
    assert_select "title", "Committees | #{@base_title}"
  end

  test "should get registration" do
    get registration_path
    assert_response :success
    assert_select "title", "Registration | #{@base_title}"
  end

  test "should get schedule" do
    get schedule_path
    assert_response :success
    assert_select "title", "Schedule | #{@base_title}"
  end

  test "should get studyguide" do
    get studyguide_path
    assert_response :success
    assert_select "title", "Study Guides | #{@base_title}"
  end
  

end
