class Contact < MailForm::Base
  attribute :name, :validate => true
  attribute :email, :validate => /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  attribute :message, :validate => true
  attribute :nickname, :captcha => true
  
 #Declare the e-mail headers. It accepts anything the mail method
  # in ActionMailer accepts.
  def headers
    {
      :subject => "Contact Form",
      :to => "mayya.rohan@gmail.com",
      :from => %("#{name}" <#{email}>)
    }
  end
end